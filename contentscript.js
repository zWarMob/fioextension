var fnar_username = "";
var fnar_password = "";

// I'm terrible at this sort of thing.  This is a complete and utter hack.
chrome.storage.sync.get("login", function (obj) 
{
	fnar_username = obj["login"];
	chrome.storage.sync.get("password", function (obj) 
	{
		fnar_password = obj["password"];
		
		// Yea, this is all awful, but :shrug:
		inject([
			scriptFromSource("var fnar_username = '" + fnar_username + "';"),
			scriptFromSource("var fnar_password = '" + fnar_password.replace(/'/g, "\\'") + "';"),
			scriptFromFile('script.js')
		]);
	});
});

function scriptFromFile(file) {
    var script = document.createElement("script");
    script.src = chrome.runtime.getURL(file);
    return script;
}

function scriptFromSource(source) {
    var script = document.createElement("script");
    script.textContent = source;
    return script;
}

function inject(scripts) {
    if (scripts.length === 0)
        return;
    var otherScripts = scripts.slice(1);
    var script = scripts[0];
    var onload = function() {
        script.parentNode.removeChild(script);
        inject(otherScripts);
    };
    if (script.src != "") {
        script.onload = onload;
        document.head.appendChild(script);
    } else {
        document.head.appendChild(script);
        onload();
    }
}