function fnar_login(fnar_username, fnar_password)
{
    var data = 
	{
        "UserName": fnar_username,
        "Password": fnar_password
    };

	var fnar_url = "https://localhost:4443";
    var url = fnar_url + "/auth/login";
    var fnarhttp = new XMLHttpRequest();

    fnarhttp.onreadystatechange = function()
    {
        if (this.readyState === XMLHttpRequest.DONE)
        {
            var status = this.status;
            if (status === 0 || (status >= 200 && status < 400))
            {
				alert("Authentication successful.");
				window.close();
				return true;
            }
			else
			{
				alert("Failed to authenticate.");
			}	
        }
    };
    fnarhttp.withCredentials = false;
    fnarhttp.open("POST", url, true);
    fnarhttp.setRequestHeader("Content-type", "application/json");
    fnarhttp.send(JSON.stringify(data));
}

var LoginController = function ()
{
	this.button_ = document.getElementById('button');
	this.login_ = document.getElementById('login');
	this.password_ = document.getElementById('password');
	this.addListeners_();
};

LoginController.prototype =
{
	button_:null,
	login_:null,
	password_:null,
	
	addListeners_: function ()
	{
		this.button_.addEventListener('click', this.handleClick_.bind(this));
	},
	
	handleClick_: function ()
	{
		chrome.storage.sync.set({"login": this.login_.value}, function (){});
		chrome.storage.sync.set({"password": this.password_.value}, function (){});
		
		fnar_login(this.login_.value, this.password_.value);
	}
};

document.addEventListener('DOMContentLoaded', function ()
{
	window.LC = new LoginController();
	chrome.storage.sync.get("login", function (obj) 
	{
		if (obj["login"] != null )
		{
			window.LC.login_.value = obj["login"];
		}
	});
	chrome.storage.sync.get("password", function (obj) 
	{
		if (obj["password"] != null )
		{
			window.LC.password_.value = obj["password"];	
		}
	});
});